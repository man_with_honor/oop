#include <set>
#include <iostream>

using namespace std;

int main() {
    int counter, limit, a;
    set <int> numbers;
    cout << "How many numbers will you enter?" << endl;
    cin >> limit;
    for(counter = 0; counter < limit; counter++) {
        cin >> a;
        numbers.insert(a);
    }
    cout << "The number of different elements is " << numbers.size() << endl;
    return 0;
}
