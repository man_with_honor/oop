#include <iostream>
#include <fstream>
#include <set>
#include <deque>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
    ifstream fin;
    deque <string> words;
    set <string> S;
    set <string>::iterator it;
    int counter1, counter2;
    string s;

    fin.open(argv[1]);
    while(!(fin.eof())) {
        fin >> s;
        words.push_back(s);
    }

    for(counter1 = 0; counter1 < words.size(); counter1++) {
        for(counter2 = 0; counter2 < words[counter1].length(); counter2++)
            if ((65 <= words[counter1][counter2]) && (words[counter1][counter2] <= 90))
                words[counter1][counter2] = words[counter1][counter2] + 32;
    }

    for(counter1 = 0; counter1 < words.size(); counter1++)
        S.insert(words[counter1]);

    for(it = S.begin(); it != S.end(); it++)
        cout << *it << endl;

    fin.close();
    return 0;
}
