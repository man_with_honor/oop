#include <iostream>
#include <map>
#include <fstream>
#include <string>

using namespace std;
//int argc, char* argv[]
int main(int argc, char* argv[]) {
    ifstream fin;
    char symbol;
    map <string, int> words;
    map <string, int>::iterator it;
    int i;
    string my_string;
    fin.open(argv[1]);

    while(!(fin.eof())) {
        fin >> my_string;
        it = words.find(my_string);
        if (it == words.end())
            words.insert(pair <string, int>(my_string, 1));
        else
            it->second++;
    }
    it->second--;
    for(it = words.begin(); it != words.end(); it++) {
      cout << it->first << " : " << it->second << endl;///вывод на экран
    }

    fin.close();
    return 0;
}
