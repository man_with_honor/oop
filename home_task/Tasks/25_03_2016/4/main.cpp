#include <iostream>
#include <set>
#include <vector>
#include <string.h>

using namespace std;

int main() {
    set <char> letters;
    char *word = "programming";
    vector <bool> my_vector;
    int counter, n, t = 8;
    char symbol;


    n = strlen(word);
    for(counter = 0; counter < n; counter++) {
        letters.insert(word[counter]);
        W.push_back(false);
    }

    while(letters.size() != 0) {
        if (t == 0) {
            cout << "You lost..." << endl;
            return 0;
        }

        cout << "The word is:" << endl;
        for(counter = 0; counter < n; counter++) {
            if (my_vector[counter])
                cout << word[counter];
            else
                cout << "*";
            }
        cout << endl;
        cout << "You have " << t << " lives" << endl;
        cout << "Enter some letter:" << endl;
        cin >> symbol;
        if (letters.erase(symbol) != 0) {
            cout << "You are right!" << endl;
            for(counter = 0; counter < n; counter++)
                if (word[counter] == symbol)
                    W[counter] = true;
        } else {
            t--;
            cout << "You are wrong!" << endl;
        }
    }
    cout << "Congratulations!" << endl;
    return 0;
}
