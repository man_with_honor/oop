#include <iostream>

using namespace std;

template <class T>
class SmartPointer {

    unsigned * ref_count;
public:
	 T * obj;
	SmartPointer(T * object){
		obj = object;
		ref_count = new unsigned;
		*ref_count = 1;
        /* WE want to set value  T * obj and send counter
         * reference to 1. */
        /* set T *obj and ref_count = 1
        */
    }

    SmartPointer(SmartPointer<T> & sptr) {
		obj = sptr.obj;
		ref_count = sptr.ref_count;
		*ref_count = (*ref_count) + 1;
        /* This construcrot create new smart pointer on existing 
         * object. We need to set obj � ref_count
         * the same as in sptr. Then,
         *  we need 
         * increase ref_count. */
         /*
        Set obj and ref_count as in sptr, increment ref_count
         */
    }

    ~SmartPointer() {
        /* Delete reference to object. Decrease
         * ref_count. If  ref_count = 0, release memory and 
         * delete object. */
         /*
         decrement ref_count. if ref_count = 0, free memory
         */
		*ref_count = (*ref_count) - 1;
		cout << "Deleting! References left: " << *ref_count << endl;
		if (*ref_count == 0) delete(obj);
    }

    SmartPointer<T> & operator=(SmartPointer<T> & sptr) {
        /* reboot =. amount of reference to old pointer
         * will be decreased, new� - increased.
         */
        /*
        decrement ref_count, set obj and ref_count, increment ref_count
        */
		(*ref_count)--;
		if (*ref_count == 0) delete(obj);
		obj = sptr.obj;
		ref_count = sptr.ref_count;
		(*ref_count)++;
		return *this;
    }

    T& operator *() {
        return *obj;
    }

    T* operator->() {
        return obj;
    }
};

class Int {
    public:
    Int(int y):x(y){}
    int x;
};


int main() {
	int *a, *b, *c;
	a = new int(3);
	b = new int(4);
	c = new int(5);
	SmartPointer <int> pointer1(a), pointer2(b), pointer3(c);
	cout << *pointer1 << *pointer2 << *pointer3 << endl;
	pointer1 = pointer2;
	*(pointer2.obj) = 1;
	cout << *pointer1 << *pointer2 << *pointer3<< endl;
	SmartPointer <int> pointer4(pointer3);
	cout << *pointer1 << *pointer2 << *pointer3 << *pointer4 << endl;
    SmartPointer<Int> A(new Int(7));
    A->x = 8;
    cout << A->x << endl;

	return 0;
}
