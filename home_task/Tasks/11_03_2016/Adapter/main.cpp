// Purpose.  Adapter design pattern lab
// 
// ������.  ����� ������������ "stack machine", ��� �� ��-����������
// ������ Stack.  �� ������ �� �������� ������������ ���� "legacy"
// ����� ��� ������ ��� ������ ������ Queue, �� ������� "�������������� ����������" 
// ����� ������ � ����� �����������.
// 
// �������.
// o ����� Queue ������ ��������� "�������" ���������� Stack.
// o ������ Queue ��������� ������ ctor, dtor(��), enque, deque, � isEmpty.  ������ ��
//   ���� ������� ��������� ��������� �� ���������� Queue � ����������
//   Stack.
// o ���� �� ���������� Queue::enque() �� Stack::push(), �� ����������� ���� ���������
//   ���� ��� ��������� ���������� ������ Queue::deque().

#include <iostream>

using namespace std;

struct StackStruct {
   int*  arr;
   int   sp;
   int   size;
};
typedef StackStruct Stack;


static void initialize( Stack* my_stack, int size ) {
   my_stack->arr = new int[size];
   my_stack->size = size;
   my_stack->sp = 0; }

static void cleanUp( Stack* my_stack ) {
   delete my_stack->arr; }

static int isEmpty( Stack* my_stack ) {
   return my_stack->sp == 0 ? 1 : 0; }

static int isFull( Stack* my_stack ) {
   return my_stack->sp == my_stack->size ? 1 : 0; }

static void push( Stack* my_stack, int item ) {
   if ( ! isFull(my_stack)) my_stack->arr[my_stack->sp++] = item; }

static int pop( Stack* my_stack ) {
   if (isEmpty(my_stack)) return 0;
   else            return my_stack->arr[--my_stack->sp]; }

class Queue {
	Stack my_stack1, my_stack2;
	public:
	Queue(int n) { //�����������
		initialize(&my_stack1, n);
		initialize(&my_stack2, n);
	}

	~Queue() {
		delete my_stack1.arr;
		delete my_stack2.arr;
	}

	void enque(int a) {
		push(&my_stack1, a);
	}

	int deque() {
		int result;
		int i;

		while (!isEmpty(&my_stack1))
			push(&my_stack2, pop(&my_stack1));
		result = pop(&my_stack2);
		while (!isEmpty(&my_stack2))
			push(&my_stack1, pop(&my_stack2));
		return result;
	} 

	int is_Empty () {
		return isEmpty(&my_stack1);
	}
  /* deque pseudocode:
      initialize a local temporary instance of Stack
      loop
         pop() the permanent stack and push() the temporary stack
      pop() the temporary stack and remember this value
      loop
         pop() the temporary stack and push() the permanent stack
      cleanUp the temporary stack
      return the remembered value
   int isFull()  { return ::isFull( &_stack );  }  */
};

int main() {
   Queue  queue(15);
	Stack my_stack;
	
   for (int i=0; i < 25; i++) queue.enque( i );
   while ( ! queue.is_Empty())
	  cout << queue.deque() << " ";
   cout << endl;


   return 0;
}

// 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 
