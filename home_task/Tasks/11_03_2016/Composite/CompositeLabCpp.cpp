#include <iostream>


using namespace std;

class Component {
	protected:
	int value;	

 	public:
	Component(int nval):value(nval){}
	virtual void traverse() = 0;
};

class Primitive: public Component {
public:
   Primitive( int val ): Component(val)  {  }
   void     traverse()                            { cout << value << " "; }
};

class Composite: public Component {
public:
   Composite( int val ): Component(val)  { total = 0; }
   void     add( Composite* c )                       { children[total++] = c; }
   void traverse() {
      cout << value << " ";


      for (int counter=0; counter < total; counter++)
          children[counter]->traverse();

   }
private:
   int         total;
   Composite*  children[99];
};

int main( void ) {
   Composite  first(1), second(2), third(3);

   first.add( &second );
   first.add( &third );
   first.add( (Composite*) new Primitive(4) );
   second.add( (Composite*) new Primitive(5) );
   second.add( (Composite*) new Primitive(6) );
   third.add( (Composite*) new Primitive(7) );
   first.traverse();
   cout << endl;
   return 0;
}

// 1 2 5 6 3 7 4
