#include <iostream>
#include <conio.h>

using namespace std;
//calss declaration
class Str {
	public:
        char *symbol;
        Str operator+ (Str first_symbol) {
		Str res;
                int n = strlen(symbol) + strlen(first_symbol.symbol) + 1;
                res.symbol = new char[n];
                strcpy(res.symbol, symbol);
                strcat(res.symbol, first_symbol.symbol);
		return res;
	}
};
//main programm
int main() {
	int i;
        Str first_symbol, second_symbol;
	cout << "Hi!" << endl;
        first_symbol.symbol = "str1";
        second_symbol.symbol = "str2";
        Str c = first_symbol + second_symbol;
        cout << c.symbol;
	getch();
	return 0;
}
