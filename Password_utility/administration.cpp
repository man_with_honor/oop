#include "administration.h"
#include "Password.h"
#include "dialog.h"
#include "ui_dialog.h"
#include <QtCore>
#include <QtGui>
#include <QCoreApplication>
#include <QDebug>
#include <iostream>
#include <QString>
#include <QFile>
#include <QMessageBox>
#include "Cipher.h"
#include <time.h>
#include "Config.h"

void b(){
    int c;
    c++;
    qDebug()<<c;
}

char Administration::to_char(bool parametr, char symbol){
      if(parametr==1) { symbol='1';}
    else { symbol='0';}
    return symbol;
    }

bool Administration:: to_bool(bool parametr, char symbol){
    if(symbol=='0'){
    parametr=0;
    } else{parametr=1;}
return parametr;
}

Config Administration::config_read(Config cfg){
    QFile file("Administration.txt");
       file.open(QIODevice::ReadOnly | QIODevice::Text);
       QList<QByteArray> rows;
       while (!file.atEnd())
           rows.append(file.readLine());
       file.close();

     cfg.number=to_bool(cfg.number,rows[4].data()[rows[4].size()-2]);
     cfg.spec_char=to_bool(cfg.spec_char,rows[5].data()[rows[5].size()-2]);
     cfg.small_letter=to_bool(cfg.small_letter,rows[6].data()[rows[6].size()-2]);
     cfg.big_letter=to_bool(cfg.big_letter,rows[7].data()[rows[7].size()-2]);
     cfg.in_dictionary=to_bool(cfg.in_dictionary,rows[8].data()[rows[8].size()-2]);
     cfg.simple_sequence=to_bool(cfg.simple_sequence,rows[9].data()[rows[9].size()-2]);
return cfg;
}

void Administration::config_output(Config cfg, QString dic ,QString base){
        QFile file("Administration.txt");
        QList<QByteArray> rows;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        while (!file.atEnd())
        rows.append(file.readLine());
        }
        else{
             QMessageBox::information(0,"Report","Administration file doesn't open. You should contact your administrator.\n");
        }

        file.close();

        dic= "Path to dictionary: " + dic +"\n";
        rows[1]=dic.toLatin1();
        base="Path to data base: " + base +"\n";
        rows[2]=base.toLatin1();

        rows[4].data()[rows[4].size()-2]= to_char(cfg.number,  rows[4].data()[rows[4].size()-2]);
        rows[5].data()[rows[5].size()-2]= to_char(cfg.spec_char,  rows[5].data()[rows[5].size()-2]);
        rows[6].data()[rows[6].size()-2]= to_char(cfg.small_letter,  rows[6].data()[rows[6].size()-2]);
        rows[7].data()[rows[7].size()-2]= to_char(cfg.big_letter,  rows[7].data()[rows[7].size()-2]);
        rows[8].data()[rows[8].size()-2]= to_char(cfg.in_dictionary,  rows[8].data()[rows[8].size()-2]);
        rows[9].data()[rows[9].size()-2]= to_char(cfg.simple_sequence,  rows[9].data()[rows[9].size()-2]);

       file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text);
       QByteArray r;
       foreach(r,rows)
           file.write(r);
       file.close();
}

void Administration:: change_admin_pswd(QString pswd){
    QFile file("Administration.txt");
    QList<QByteArray> rows;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
    while (!file.atEnd())
    rows.append(file.readLine());
    }
    else{
         QMessageBox::information(0,"Report","Administration file doesn't open. You should contact your administrator.\n");
    }
    file.close();

    pswd= "Password: " + pswd +"\n";
    rows[0]=pswd.toLatin1();

    file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text);
    QByteArray r;
    foreach(r,rows)
        file.write(r);
    file.close();
}


QString Administration:: receive_admin_pswd(){
    QString admin_pswd;
    QFile file("Administration.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
        {
                QString line = file.readLine();
                QStringList lst = line.split(" ");
                admin_pswd=lst[1];
                admin_pswd=admin_pswd.mid(0,admin_pswd.size()-1);
        }
        else
        {
           QMessageBox::information(0,"Report","Administration file doesn't open. You should contact your administrator.\n");
        }
                file.close();
                return admin_pswd;
}

QString Administration::receive_path_to_dictionary(){
    QString path_to_dic;
    QFile file("Administration.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
        {
                QString line = file.readLine();
                line = file.readLine();
                QStringList lst = line.split(" ");
                path_to_dic=lst[3];
                path_to_dic=path_to_dic.mid(0,path_to_dic.size()-1);
        }
        else
        {
           QMessageBox::information(0,"Report","Administration file doesn't open. You should contact your administrator.\n");
        }
                file.close();
                return path_to_dic;
}
QString Administration:: receive_path_to_data_base(){
    QString path_to_base;
    QFile file("Administration.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
        {
                QString line = file.readLine();
                line = file.readLine();
                line = file.readLine();
                QStringList  lst = line.split(" ");
                path_to_base=lst[4];
                path_to_base=path_to_base.mid(0,path_to_base.size()-1);
        }
        else
        {
             QMessageBox::information(0,"Report","Administration file doesn't open. You should contact your administrator.\n");

        }
    file.close();
    return path_to_base;
}
