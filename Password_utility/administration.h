#ifndef ADMINISTRATION_H
#define ADMINISTRATION_H

#include <QString>
#include "Config.h"


class Administration
{
private:
    char to_char(bool parametr, char symbol);
    bool to_bool(bool parametr, char symbol);

public:
    Config config_read(Config cfg);
    void config_output(Config cfg, QString dic ,QString base);
    QString receive_path_to_dictionary();
    QString receive_path_to_data_base();
    QString receive_admin_pswd();
    void change_admin_pswd(QString pswd);
};


#endif // ADMINISTRATION_H
